from django import forms
from tendenci.apps.forums.models import Forum, Category


class CreateForm(forms.ModelForm):
    class Meta:
        model = Forum
        fields = [
            'name',
            'category',
        ]

    def __init__(self, *args, **kwargs):
        super(CreateForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(
            status='True')


class CreateCategory(forms.ModelForm):
    STATE_CATEGORY = (
        ('', '---------'),
        ('0', 'Inactive'),
        ('1', 'Active'),
    )
    name = forms.CharField(
        label='Category Name'
    )
    status_detail = forms.ChoiceField(
        required=True,
        label='Category Status',
        choices=(('active', ('Active')), ('inactive', ('Inactive')),)
    )

    def __init__(self, *args, **kwargs):
        super(CreateCategory, self).__init__(*args, **kwargs)
        self.fields['status_detail'].choices.insert(0, ('', '---------'))

    class Meta:
        model = Category
        fields = ('name', 'status_detail', 'allow_anonymous_view')
