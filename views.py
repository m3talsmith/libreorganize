from django.shortcuts import render
from django.conf import settings
from django.shortcuts import render, get_object_or_404, redirect
from tendenci.apps.theme.shortcuts import themed_response as render_to_resp
from tendenci.apps.forums.models import (
    Category,
    Forum
)
from tendenci.apps.accounts.forms import LoginForm
from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    DeleteView
)
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from .forms import (
    CreateForm,
    CreateCategory
)
from django.contrib import messages
# Create your views here.


class ForumList(LoginRequiredMixin, SuccessMessageMixin, ListView):
    template_name = 'forum/forum_list.html'
    context_object_name = 'forums'

    def get_queryset(self):
        return Forum.objects.order_by('-updated')

    def get_context_data(self, **kwargs):
        context = super(ForumList, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.order_by(
            'name').filter(status='True')
        return context


class CategoryCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'forum/forum_category.html'
    model = Category
    success_message = 'Category Created!'
    form_class = CreateCategory
    success_url = reverse_lazy('libreorganize:forum_list')

    def form_valid(self, form):
        form.instance.creador = self.request.user
        return super().form_valid(form)


class ForumCreate(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Forum
    form_class = CreateForm
    template_name = 'forum/forum_create.html'
    success_message = "Forum Created!"
    success_url = reverse_lazy('libreorganize:forum_list')

    def form_valid(self, form):
        form.instance.creador = self.request.user
        return super().form_valid(form)


class ForumUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Forum
    form_class = CreateForm
    template_name = 'forum/forum_update.html'
    success_message = "Forum Updated"
    success_url = reverse_lazy('libreorganize:forum_list')

    def form_valid(self, form):
        form.instance.creador = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.object.name
        return context


class CategoryUpdate(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Category
    form_class = CreateCategory
    template_name = 'forum/forum_category_update.html'
    success_message = "Category Updated!"
    success_url = reverse_lazy('libreorganize:forum_list')

    def form_valid(self, form):
        form.instance.creador = self.request.user
        return super().form_valid(form)


def login(request, form_class=LoginForm, template_name="account/login.html"):

    redirect_to = request.GET.get('next', u'')

    if request.method == "POST":
        default_redirect_to = getattr(settings, "LOGIN_REDIRECT_URLNAME", None)
        if default_redirect_to:
            default_redirect_to = reverse(default_redirect_to)
        else:
            default_redirect_to = settings.LOGIN_REDIRECT_URL

        # light security check -- make sure redirect_to isn't garabage.
        if not redirect_to or "://" in redirect_to or " " in redirect_to:
            redirect_to = default_redirect_to

        form = form_class(request.POST, request=request)
        if form.login(request):
            EventLog.objects.log(instance=request.user, application="accounts")

            return HttpResponseRedirect(redirect_to)
    else:
        form = form_class(request=request)

        if request.user.is_authenticated and redirect_to:
            return HttpResponseRedirect(redirect_to)

    return render_to_resp(request=request, template_name=template_name, context={
        "form": form
    })


def forum_delete(request, slug=None):
    forum = get_object_or_404(Forum, slug=slug)
    forum.delete()
    messages.error(request, 'Forum Deleted!')
    return redirect('libreorganize:forum_list')


def category_delete(request, slug=None):
    category = get_object_or_404(Category, slug=slug)
    category.delete()
    messages.error(request, 'Category Deleted!')
    return redirect('libreorganize:forum_list')
