from django.conf.urls import url
from . import views

app_name = 'libreorganize'

urlpatterns = [
    url('^login/$', views.login, name='login'),
    url('^forums/forum/create/', views.ForumCreate.as_view(), name='forum_create'),
    url('^forums/forum/(?P<slug>[\w-]+)/update/$', views.ForumUpdate.as_view(), name='forum_update'),
    url('^forums/forum/(?P<slug>[\w-]+)/delete/$', views.forum_delete, name='forum_delete'),    
    url('^forums/category/create/', views.CategoryCreate.as_view(), name='category_create'),
    url('^forums/category/(?P<slug>[\w-]+)/update/$', views.CategoryUpdate.as_view(), name='category_update'),
    url('^forums/category/(?P<slug>[\w-]+)/delete/$', views.category_delete, name='category_delete'),
    url('^forums/forum_list/', views.ForumList.as_view(), name='forum_list'),
]