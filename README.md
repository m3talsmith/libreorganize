
  

#  Set up LibreOrganize App

  

  

**You'll need to clone this branch in your instance:**

  

git clone https://gitlab.com/libreorganize/libreorganize libreorganize

  
  

**Install requirements after you active your virtualenv:**

  

    pip install -r requirements.txt

  

**Add to installed apps in**  `conf/settings.py`

  

    INSTALLED_APPS += ['libreorganize', 'crispy_forms', ]

 
 **And then add the following urls to pre_patterns in** `conf/urls.py` 
  
  

    url(r'^libreorganize/',  include('libreorganize.urls')),

  

**Final add at the bottom of**  `conf/settings.py`

  

    CRISPY_TEMPLATE_PACK = 'bootstrap3'
